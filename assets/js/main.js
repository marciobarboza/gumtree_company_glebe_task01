var inputData;
var buttonSave;
var listElement;
var dictionary;
var userFeedback;

function start() {
    loadElements();
}

function loadElements() {

    //init array
    dictionary = new Array();

    //load button element and click event
    buttonSave = document.getElementById('save');
    buttonSave.addEventListener('click', addData);

    //load input
    inputData = document.getElementById('insert');
    inputData.focus();

    //userFeedback
    userFeedback = document.getElementById('user_feedback');

    //load list element
    listElement = document.getElementById('list');
}

function addData() {
    userFeedback.innerText = '';
    if(inputData.value.length > 0) {
        dictionary.push(reverseString(inputData.value));
        inputData.value = '';
        loadList();
    } else {
        userFeedback.innerText = 'Insert an item and press \"save\"';
    }
    inputData.focus();
}

function loadList() {

    //remove all child from listElement
    while( listElement.firstChild ){
       listElement.removeChild( listElement.firstChild );
    }

    //add all child from array
    dictionary.forEach(function(element) {
        var itemElement = document.createElement('li');
        itemElement.innerText = element;
        listElement.appendChild(itemElement);
    }, this);
}

function reverseString(str) {
    // Step 1. Use the split() method to return a new array
    var splitString = str.split("");
    // ["h", "e", "l", "l", "o"]
 
    // Step 2. Use the reverse() method to reverse the new created array
    var reverseArray = splitString.reverse();
    // ["o", "l", "l", "e", "h"]
 
    // Step 3. Use the join() method to join all elements of the array into a string
    var joinArray = reverseArray.join("");
    
    //Step 4. Return the reversed string
    return joinArray;
}